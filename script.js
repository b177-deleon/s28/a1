fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {json.map((todos) => console.log(todos.title)
)});

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" has a status of ${json.completed}`));

fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created to-do list item',
		completed: false,
		userId: 1
	}),	
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to-do list item',
		description: 'To update my to-do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	}),	
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Completed',
		completed: true,
		dateCompleted: '2022-06-01'
	}),	
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'DELETE'
})
